import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Events } from '../models';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from './app.config.service';

@Injectable()
export class EventService {

    apiUrl = AppConfig.settings.apiUrl;
    constructor(private http: HttpClient) { }

    public eventsSource = new Subject<Events>();
    public events$ = this.eventsSource.asObservable();


    sendEvent(ev: Events) {
        setTimeout(() => {
            this.eventsSource.next(ev);
        }, 0);
    }

    getAll() {
        return this.http.get<Events[]>(`${this.apiUrl}/events`);
    }

    addEvent(ev: Events) {
        return this.http.post(`${this.apiUrl}/events/add`, ev);
    }

    delete(id: number) {
        return this.http.delete(`${this.apiUrl}/events/${id}`);
    }

    editEvent(ev: Events) {
        return this.http.put(`${this.apiUrl}/events/edit`, ev);
    }

}