import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { User, Events } from '../models';


let users: Array<User> = JSON.parse(localStorage.getItem('users')) || [];
let events: Array<Events> = JSON.parse(localStorage.getItem('events')) || [];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body } = req;

        return of(null)
            .pipe(mergeMap(handleRoute))
            .pipe(materialize())
            .pipe(delay(500))
            .pipe(dematerialize());

        function handleRoute() {
            switch (true) {
                case url.endsWith('/users/authenticate') && method === 'POST':
                    return authenticate();
                case url.endsWith('/users/register') && method === 'POST':
                    return register();
                case url.endsWith('/users') && method === 'GET':
                    return getUsers();
                case url.match(/\/users\/\d+$/) && method === 'DELETE':
                    return deleteUser();
                case url.endsWith('/events') && method === 'GET':
                    return getEvents();
                case url.endsWith('/events/add') && method === 'POST':
                    return addEvent();
                case url.endsWith('/events/edit') && method === 'PUT':
                    return editEvent();
                case url.match(/\/events\/\d+$/) && method === 'DELETE':
                    return deleteEvent();
                default:
                    // pass through any requests not handled above
                    return next.handle(req);
            }
        }

        function authenticate() {
            const { username, password } = body as User;
            const user = users.find(x => x.username === username && x.password === password);

            if (!user) return error('Username or password is incorrect');
            let okBod: User = {
                id: user.id,
                username: user.username,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                phone: user.phone,
                token: 'fake-jwt-token'
            }
            return ok(okBod);
        }

        function register() {
            const user: User = body;

            if (users.find(x => x.username === user.username)) {
                return error(`Username ${user.username} is already taken!`);
            }

            user.id = users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;
            users.push(user);

            localStorage.setItem('users', JSON.stringify(users));

            return ok();
        }


        function getUsers() {
            if (!isLoggedIn()) return unauthorized();
            return ok(users);
        }

        function deleteUser() {
            if (!isLoggedIn()) return unauthorized();

            users = users.filter(x => x.id !== idFromUrl());
            localStorage.setItem('users', JSON.stringify(users));
            return ok();
        }

        function addEvent() {
            if (!isLoggedIn()) return unauthorized();
            const ev: Events = body;

            if (events.find(x => x.name === ev.name)) {
                return error(`Name ${ev.name} is already taken!`);
            }

            ev.id = events.length ? Math.max(...events.map(x => x.id)) + 1 : 1;
            events.push(ev);

            localStorage.setItem('events', JSON.stringify(events));

            return ok();
        }

        function getEvents() {
            if (!isLoggedIn()) return unauthorized();
            return ok(events);
        }

        function editEvent() {
            const ev: Events = body;
            events = events.map(x => x.id === ev.id ? ev : x);

            localStorage.setItem('events', JSON.stringify(events));

            return ok();
        }

        function deleteEvent() {
            if (!isLoggedIn()) return unauthorized();

            events = events.filter(x => x.id !== idFromUrl());

            localStorage.setItem('events', JSON.stringify(events));
            return ok();
        }


        // helpers

        function ok(body?) {
            return of(new HttpResponse({ status: 200, body }))
        }

        function error(message) {
            return throwError({ error: { message } });
        }

        function unauthorized() {
            return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }

        function isLoggedIn() {
            return headers.get('Authorization') === 'Bearer fake-jwt-token';
        }

        function idFromUrl() {
            const urlParts = url.split('/');
            return parseInt(urlParts[urlParts.length - 1]);
        }
    }

}

export const fakeBackendProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};