import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { AuthenticationService } from '../services';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authSerive: AuthenticationService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const currentUser = this.authSerive.currentUserValue;

    // logged in
    if (currentUser) {
      return true;
    }

    // not logged in
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });

    return false;
  }

}
