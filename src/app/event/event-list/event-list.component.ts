import { Component, OnInit, OnDestroy } from '@angular/core';
import { Events } from 'src/app/core/models';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { EventDeleteDialogComponent } from '../event-delete-dialog/event-delete-dialog.component';
import { EventService } from 'src/app/core/services';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit, OnDestroy {

  displayedColumns: string[] = ['name', 'date', 'description', 'edit'];
  dataSource: Events[];
  getEventSubscription: Subscription;

  constructor(private router: Router, public dialog: MatDialog, private eventService: EventService) { }

  ngOnInit() {
    this.eventService.getAll().pipe(first()).subscribe(data => {
      this.dataSource = data;
    });
  }

  edit(ev: Events) {
    this.router.navigate(['/edit']);
    this.eventService.sendEvent(ev);
  }

  delete(ev: Events) {
    const dialogRef = this.dialog.open(EventDeleteDialogComponent, {
      width: '450px',
      data: ev
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.eventService.delete(result.id)
          .pipe(first())
          .subscribe(data => {
            this.dataSource = this.dataSource.filter(x => x !== result);
          }, error => {

          });
      }
    });
  }

  add() {
    this.router.navigate(['/add']);
  }

  ngOnDestroy(): void {
    if (this.getEventSubscription) {
      this.getEventSubscription.unsubscribe();
      this.getEventSubscription = null;
    }
  }

}
