import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router,private auth: AuthenticationService) { }

  ngOnInit() {
    this.router.navigate(['/list']);
  }

  navAdd(){
    this.router.navigate(['/add']);
  }

  navList(){
    this.router.navigate(['/list']);
  }

  logout(){
    this.auth.logout();
    this.router.navigate(['/login']);
  }
}
