import { Component, OnInit, OnDestroy, } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from 'src/app/core/services';
import { Subscription } from 'rxjs';
import { Events } from 'src/app/core/models';
import { first } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-event-add',
  templateUrl: './event-add.component.html',
  styleUrls: ['./event-add.component.css']
})
export class EventAddComponent implements OnInit, OnDestroy {

  eventForm: FormGroup;
  addBool: boolean = true;
  editSubscription: Subscription;
  editHasData: boolean = false;
  recivedEdit: Events;

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private eventService: EventService,
    private router: Router,
    private _snackBar: MatSnackBar) {
    this.eventForm = this.fb.group({
      name: ['', Validators.required],
      date: ['', Validators.required],
      description: ['', Validators.required]
    });

    this.editSubscription = this.eventService.events$.subscribe(ev => {
      this.eventForm.setValue({
        name: ev.name,
        date: ev.date,
        description: ev.description
      })
      this.editHasData = true;
      this.recivedEdit = ev;
    });
  }

  ngOnInit() {
    this.addBool = this.route.snapshot.url.some(x => x.path === 'add');
  }

  add() {
    if (this.eventForm.invalid) {
      this.eventForm.markAllAsTouched();
      return;
    }

    this.eventService.addEvent(this.eventForm.value)
      .pipe(first())
      .subscribe(data => {
        this.router.navigate(['/list']);
      },
        error => {
          this._snackBar.open(error, '', { duration: 1000 });
        });

  }

  edit() {
    if (this.eventForm.invalid) {
      this.eventForm.markAllAsTouched();
      return;
    }

    this.recivedEdit = {
      ...this.recivedEdit,
      ...this.eventForm.value
    };
    this.eventService.editEvent(this.recivedEdit)
      .pipe(first())
      .subscribe(data => {
        this.router.navigate(['/list']);
      },
        error => {

        });
  }

  ngOnDestroy(): void {
    if (this.editSubscription) {
      this.editSubscription.unsubscribe();
      this.editSubscription = null;
    }
  }

}
