import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { EventAddComponent } from './event/event-add/event-add.component';
import { EventListComponent } from './event/event-list/event-list.component';
import { AuthGuard } from './core/utils';
import { HomeComponent } from './event/home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'add',
        component: EventAddComponent
      },
      {
        path: 'edit',
        component: EventAddComponent
      },
      {
        path: 'list',
        component: EventListComponent
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
